# leakytube

your tube's leaky

## Installation

compile the bin or use one of the releases

## Usage

```
  usage: leakyt [command] [args]

  command:
    download    download youtube video
    search      search youtube
    help        show this help
```

downloading a youtube vid:

```shell
leakyt dl -u <url> -p <path>
```

## Development

it's my first time for oop.
but i tried my best not to go against the lang.

## Contributing

1. Fork it (<https://gitlab.com/gwu/ytcr/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [gwimm](https://gitlab.com/gwu) - creator, don't expect me to maintain well.
