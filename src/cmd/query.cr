class Command
  def search
    query = opts.first?
    abort "no query given" unless query

    results = YouTube.search query
    abort "no results for:" + query unless results

    i = 0
    results.each do |result|
      puts "#{i.to_s}  #{result.xpath_node("h3/a").not_nil!.["title"]}"
      i += 1
    end

    sel = STDIN.gets.not_nil!
    puts results[sel.to_i].xpath_node("h3/a").not_nil!.["title"]
  end
end
