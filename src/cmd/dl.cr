require "option_parser"
require "file_utils"

class Command
  private def download
    tag : Nil | String = nil
    path = Dir.current
    idv = [] of String
    dump_url = false
    pipe = false

    OptionParser.parse(opts) do |parser|
      parser.banner = "yt.cr youtube-dl but crystal clear"

      parser.on("-u <url>", "--url <url>", "specify video url or id") do |url|
        id = YouTube.to_ytid url
        abort "ERR: #{url}: is not a youtube url." unless id
        idv.push(id)
      end
      parser.on("-t <tag>", "--tag <tag>", "specify format tag") { |t| tag = t }
      parser.on("-o <path>", "--output <path>", "specify location other than './'") { |p| path = p }
      parser.on("-d", "--dump-url", "dump url and exit") { dump_url = true }
      parser.on("-p", "--pipe", "dump url and exit") { pipe = true }
      parser.on("-h", "--help", "show this help") {
        abort parser
      }
    end

    abort "ERR: specify at least one video" if idv.empty?

    idv.each do |id|
      STDERR.puts "fetching: #{id}"
      vid = YouTube::Video.get_by_id(id)
      abort "no video found" unless vid

      STDERR.puts vid.data["videoDetails"]["title"]
      unless tag
        abort "to pipe, specify format tag with '-t'" if pipe
        tag = choose_tag(vid)
      end

      abort "invalid tag" unless tag

      if dump_url
        puts vid.tag_url(tag)
        exit
      end

      vid.tag_dl(tag.not_nil!, path)
    end
  end

  private def choose_tag(vid)
    STDERR.puts <<-header
    ─────────────────────────────────────────────┐
      tag   mime                          codecs │
    header

    vid.formats.group_by do |f|
      f["audioQuality"]? && f["qualityLabel"]? && "audio & video" ||
        f["audioQuality"]? && "audio" ||
        f["qualityLabel"]? && "video"
    end.each do |type, fv|
      STDERR.puts <<-border
      ┌────────────────────────────────────────────┘
      border
      fv.each do |f|
        mime = f["mimeType"].to_s.split("; codecs=")
        mime[1] = mime[1].strip '"'
        STDERR.printf "│ %3s │ %-10s %25s │\n", f["itag"].to_s, mime[0], mime[1]
      end
    end

    STDERR.puts "┌────────────────────────────────────────────┘"
    print "│ choose tag: " if STDOUT.tty?
    tag = STDIN.gets
    STDERR.puts <<-border
    └─────────────────────────────────────────────
    border

    return tag
  end
end
