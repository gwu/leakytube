require "json"
require "xml"
require "http"
require "html"
require "uri"

module YouTube
  extend self

  def to_ytid(vid)
    return vid if vid.size == 11

    uri = URI.parse(vid)

    host = uri.host
    return nil unless host
    path = uri.path
    return nil unless path
    return path.lchop('/') if host.includes? "youtu.be"

    query = uri.query
    return nil unless query

    vid = query.split("?").find(&.starts_with?("v="))
    return nil unless vid
    return vid.split("=")[1]
  end

  def search(query : String)
    return nil if query.empty?
    uri = URI.parse("https://www.youtube.com/results")
    uri.query = "search_query=" + URI.escape(query)
    XML.parse_html(HTTP::Client.get(uri).body.not_nil!)
      .xpath_nodes("//div[@class='yt-lockup-content']")
  end

  class Video
    property config
    property data
    property formats

    @@endpoint = "https://www.youtube.com"

    def initialize(@data : JSON::Any, @config : JSON::Any,
                   @formats : Array(JSON::Any))
    end

    def tag_url(tag)
      @formats.find { |f| f["itag"].to_s == tag } .not_nil!["url"].to_s
    end

    def tag_dl(tag : String, loc)
      uri = URI.parse(tag_url tag)
      return unless uri
      format = @formats.find { |f| f["itag"].to_s == tag } .not_nil!

      if loc != "-"
        ext = format["mimeType"].to_s.match(/(?<=\/).*(?=;)/).not_nil![0]
        name = @data["videoDetails"]["title"].to_s.gsub(/[\/:*\?<>\|]/, '_')
        video_path = File
          .join({loc, @data["videoDetails"]["author"].to_s, "#{name}.#{ext}"})
        FileUtils.mkdir_p File.dirname(video_path)
      end

      dl(uri, video_path.not_nil!, format["contentLength"].to_s.to_i.not_nil!)
    end

    private def dl(uri, loc, lim)
      file = File.new(loc, "w")
      client = HTTP::Client.new(uri)
      # TODO find out what the fuck is happening here...
      # "Error reading socket: Connection reset by peer"
      begin
        client.get(uri.full_path) do |stream|
          copy(stream.body_io, file, lim)
        end
      rescue ex
        file.close
        puts ex
      end
    end

    private def copy(src, dst, lim)
      buffer = uninitialized UInt8[4096]
      count = 0
      while (len = src.read(buffer.to_slice).to_i32) > 0 && lim > count
            dst.write buffer.to_slice[0, len]
        count += len
      end
      len < 0 ? len : count
    end

    def self.get_by_id(id : String)
      response = HTTP::Client.get(URI.parse("#{@@endpoint}/watch?v=#{id}")).body
      return nil unless response

      script = XML.parse_html(response)
        .xpath_nodes("//div[@id='player-mole-container']/script")
        .find(&.content.includes? "ytplayer.config =")
        .try(&.content)
      return nil unless script

      config = JSON.parse(
        script
          .match(/(?<=ytplayer\.config =).*?(?=;ytplayer\.load = function())/)
          .not_nil![0]
      )
      return nil unless config

      data = JSON.parse config["args"]["player_response"].to_s
      streaming_data = data["streamingData"]
      formats = streaming_data["formats"].as_a + streaming_data["adaptiveFormats"].as_a

      self.new(data, config, formats)
    end
  end
end
