require "./lib/yt"
require "./cmd/*"

Command.run

class Command
  USAGE = <<-USAGE
  usage: yt [command] [args]

  command:
    download    download youtube video
    search      search youtube
    help        show this help
  USAGE

  private getter opts

  def initialize(@opts : Array(String))
  end

  def self.run(opts = ARGV)
    new(opts).run
  end

  def run
    opt = opts.first?
    case
    when !opt
      STDERR.puts USAGE
      exit 1
    when "download" == opt, "dl" == opt
      opts.shift
      download
    when "search" == opt, "s" == opt
      opts.shift
      search
    when "help" == opt, "--help" == opt, "-h" == opt
      opts.shift
      STDERR.puts USAGE
      exit
    else
      STDERR.puts "unknown opt: #{opt}"
      exit
    end
  end
end
